
    ////*****************************NAVIGATION CODE ABOVE********************* 

var person = [], account = [], share = [], myData = [],rowSelectedRecord,oneStep = [], twoStep = [], rowSelectedPush = [], accountTargetParentSerial = [];



//Main Code starts from here
Ext.onReady(function() {

    var workTaskSerial = CR.Script.workTaskSerial;
    console.log('workTaskSerial: ' , workTaskSerial);
    
  getAccountSerial();
  
  function getAccountSerial (){
    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');
    var step = xml.addContainer(transaction, 'step');
    var record = xml.addContainer(step, 'workFlowRequest');
    xml.addOption(record, 'operation', 'VIEW');
    xml.addText(record, 'tableName', 'WORK_TASK');
    xml.addText(record, 'targetSerial', workTaskSerial);

    CR.Core.ajaxRequest({
      url: 'DirectXMLPostJSON',
      xmlData: xml.getXMLDocument(),
      success: function(response) {
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                }
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
          CR.Core.displayExceptions({ items: errorArray });
        } else {
          console.log('workflowRequest details: ', query);
          if(query.sequence[0].transaction[0].step[0].workFlowRequest.workFlowDescription === 'Add Non-Checking to Exisiting Account' ){
                accountSerial = query.sequence[0].transaction[0].step[0].workFlowRequest.workFlowExpansion.workFlow[3].results.property[1].property[0].contents;
                accountNumber = query.sequence[0].transaction[0].step[0].workFlowRequest.workFlowExpansion.workFlow[3].results.property[1].property[0].contentsDescription;        
          } else if(query.sequence[0].transaction[0].step[0].workFlowRequest.workFlowDescription === 'New Account - New/Existing Member' ){
                accountSerial = query.sequence[0].transaction[0].step[0].workFlowRequest.workFlowExpansion.workFlow[4].results.property[1].property[0].contents;
                accountNumber = query.sequence[0].transaction[0].step[0].workFlowRequest.workFlowExpansion.workFlow[4].results.property[1].property[0].contentsDescription;         
          }           
          console.log('Workflow Account Info | Serial: ' + accountSerial + ', Number: ' + accountNumber );
          accountNumberDetails();  
        }
      }
    });
}
  
/**************************************************** Account Number Details Method Start ************************************************/
function accountNumberDetails(){
   
   var navigation_data = {
        complete : 'C',
        incomplete : 'I',
        message_to_parent : {
             scriptPanelId : CR.Script.scriptDefaultPanelId,    //required
             updateApplicationContinue : true,                  //true to continue, false to cancel
             workflowStepStatus : 'C'                           // C=Complete, I=Incomplete
        },
        next:false,
        prev:false
    };

    function setAllRecords(){
        insertMaturitySavingsTransfer();
    }
        
    function letsGetOutofHere (status) {
        navigation_data.message_to_parent.updateApplicationContinue = true;
        navigation_data.message_to_parent.workflowStepStatus = status;
        console.log('LETS GETS OUTTA HERE top.postmessage :',top);
        top.postMessage(navigation_data.message_to_parent, top.location.href);
        console.log("Goodbye, I'm out of here! Status: " + status);
    }

    var myPostMessageReceiver = function (event) {   
        var message_to_script = event.data;     
        console.log('Message from core: ' + JSON.stringify(message_to_script));        
        if (message_to_script.updateApplication){
            if (message_to_script.nextButtonClick) {
                setAllRecords();
                navigation_data.next = true;
                //letsGetOutofHere(navigation_data.complete);
            } else if (message_to_script.previousButtonClick) {
                navigation_data.prev = true;
                letsGetOutofHere(navigation_data.complete);
            }            
        }
    };

    window.addEventListener('message', myPostMessageReceiver, false);

   
    var  savingsDescription = [], description = [], rowSelected; 
    
    /******************************************************* Share Details *****************************************************************/
    
    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');

    var step = xml.addContainer(transaction, 'step');
    var recordTree = xml.addContainer(step, 'recordTree');
    xml.addText(recordTree, 'tableName', 'ACCOUNT');
    xml.addText(recordTree, 'viewName', 'STANDARD');
    xml.addText(recordTree, 'targetSerial', accountSerial);
    xml.addOption(recordTree, 'includeRecordDetail', 'Y');
    var recordDetail = null;
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'SHARE');
    CR.Core.ajaxRequest({
      url: 'DirectXMLPostJSON',
      xmlData: xml.getXMLDocument(),
      success: function(response) {
        var recordTreeResponse = null;
        var records = [];
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                } else if (step.recordTree) {
                  recordTreeResponse = step.recordTree;
                } else if (step.record) {
                  records.push(step.record);
                }
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
          CR.Core.displayExceptions({ items: errorArray });
        } else {
          //console.log('recordTreeResponse', recordTreeResponse);
         // console.log('records', records);
          shareDetails(records);
        }
      }
    });

    function shareDetails(records){
        var personDetails = []; 
        personDetails = records;   

        personDetails.forEach(function (entry){      
            if(entry.tableName === 'SHARE'){
                share.push(entry);
            }
        });
      //  console.log('Share:', share);
        termCertificateShareDetails (share);
    }
    
/******************************************************* Share Details *****************************************************************/
    function  termCertificateShareDetails (share){
        var uniqueId  = 0, toolbarId = 0;
        description = [ 
                    ['Term Certificate 3-5 Month QTR DIV',3 , '3-5'],
                    ['Term Certificate 6-11 Month QTR DIV',6, '6-11'],
                    ['Term Certificate 12-23 Month QTR DIV',12, '12-23'],
                    ['Term Certificate 24-35 Month QTR DIV',24 , '24-35'],
                    ['Term Certificate 36-59 Month QTR DIV',36 , '36-59'],
                    ['Term Certificate 60-120 Month QTR DIV',60, '60-120'],
                    ['Term Certificate Add-On 12 Month QTR DIV',12, '12'],
                    ['Term Certificate Opt Rate 13 Mo QTR DIV',13 , '13'],
                    ['Term Certificate Opt Rate 24 Mo QTR DIV',24 , '24'],
                    ['Term Certificate Opt Rate 36 Mo QTR DIV',36, '36'] ];
                                   
        var store = new Ext.data.SimpleStore({
            fields: [
                {name: 'shavingDefaultDescription', type: 'string'},
                {name: 'maturityPeriod', type: 'string'},
                {name: 'maturityDate', type: 'string'}
            ]
        });
        store.loadData(description);
        var savingsId = []; 
        console.log('Share testing two:', share);

        share.forEach(function(entry){     
            if(entry.field[6].newContents >= 1000){
                savingsId.push(entry.field[6].newContents);
            } 
        });
        savingsDescription = new Ext.grid.GridPanel({
            title: 'All Defaults',
            collapsible: true,
            autoHeight : true,
            region: 'west',
            margins: '5 0 0 0',
            store: store,
            style: {cursor: 'pointer'},
            sm: new Ext.grid.RowSelectionModel({
                        //singleSelect: true,
                        selModel: {mode: 'MULTI'}, 
                        listeners: {                       
                            rowselect: function(sm, row, rec) {
                                rowSelected = rec;  
                                console.log('savings Id: ' ,savingsId);
                                var savingsIdToInt = savingsId.map(Number);                                
                                var highestId = Math.max.apply(null, savingsIdToInt);
                                console.log('converting String to Int value: ' ,highestId);                               
                                var idIncrement;  
                                var Id ;
                                 if(savingsIdToInt.length >= 1){
                                     Id = highestId;
                                 } else {
                                     Id = 999 ;
                                 }                  
                                console.log('Id for Savings Id: ' ,Id); //dont comment important console
                                if(rowSelected){
                                    Id++;
                                    uniqueId++;
                                    toolbarId++;
                                    
                                }
                                console.log('Unique Id for Form panel: ', uniqueId);
                                idIncrement = Id++;
                                console.log('increment Id: ' ,idIncrement); //dont comment important console
                                savingsId.push(idIncrement); 
                                formPanelDetails(rowSelected,idIncrement, uniqueId, toolbarId);                           
                            }
                        }
                    }),
            columns: [{
              id:'description',
              header: 'Term Certificates',
              width: 300,
              sortable: true,
              dataIndex: 'shavingDefaultDescription'
            }],
            stripeRows: true,
            height: 600,
            width: 300        
        });
        savingsDescription.show();


        CR.Core.viewPort = new Ext.Viewport({
            renderTo: Ext.getBody(),
                layout:'border',
                bodyBorder: false,
                defaults: {
                    collapsible: true,
                    split: true,
                    bodyPadding: 15
                },
                autoScroll: true,
                items:[ savingsDescription]                        
        });
    }  
         


/****************************************************form Panel Details Method Start ************************************************/
var formtest ;
var forms = {};
function formPanelDetails(rowSelected, id, uniqueId, toolbarId){
      
    /* 
     * dockedItems: [{
    xtype: 'toolbar',
    dock: 'bottom',
    items: [
        { xtype: 'button', text: 'Button 1' }
    ]
}]
     */
    
    
    formtest = new Ext.form.FormPanel({
        renderTo:Ext.getBody(),
        collapsible: true,
        closable:true,
        region: 'east',
        margins: '0 0 0 5',
        x:305,       
        tools: [{
                id: uniqueId.toString(),
                type: 'close',
                qtip: 'Remove',
                handler: function(event, toolEl, panel) {
                                panel.destroy();
                                console.log('Panel Details ' ,panel);
                                console.log('formtest: ',formtest);
                }
        }],
        style: {position:'relative' , width: 700, left:310 , cursor: 'pointer'},
        labelAlign: 'top',
        id: uniqueId,
       // name: 'form',
        frame:true,
        labelWidth: 175,
        title: rowSelected.data.shavingDefaultDescription,
        bodyStyle:'padding:5px 5px 0',
        autoHeight : true, 
        buttonAlign:'left',
        items: [{
            layout:'column',
            width :700,
            items:[{
                columnWidth:.3,
                layout: 'form',
                items: [{
                    xtype:'textfield',
                    fieldLabel: 'Savings ID',
                    name: 'savings',                   
                    anchor:'95%',
                    value : id,
                    allowBlank: false
                },{
                    xtype:'textfield',
                    fieldLabel: 'Description',
                    name: 'desc',
                    anchor:'95%',
                    value : rowSelected.data.shavingDefaultDescription
                },{
                    xtype:'crOptionField',
                    crColumnDescription: 'Maturity Frequency',                   
                    crColumnName: 'maturityFrequency',
                    crOptions : [
                                    ["M","Months"],
                                    ["D","Days"]
                                ],
                    anchor:'95%'
                }]
            },{
                columnWidth:.3,
                layout: 'form',
                items: [{
                    xtype:'crOptionField',
                    crColumnDescription: 'Dividend Posting',
                    crColumnName: 'dividendPosting',
                    crOptions : [
                                    ["D","Deposit"],
                                    ["T","Transfer"]
                                ],
                    anchor:'95%'
                },{
                    xtype:'crOptionField',
                    crColumnDescription: 'Maturity Posting',
                    crColumnName: 'maturityPosting',
                    crOptions : [
                                    ["R","Renew"],
                                    ["T","Transfer then close"]
                                ],
                    anchor:'95%'                    
                },{
                    xtype:'textfield',
                    fieldLabel: 'Maturity Period',
                    name: 'period',
                    value: rowSelected.json[1],
                    anchor:'95%'                   
                }]
            },{
                columnWidth:.3,
                layout: 'form',
                items: [{
                    xtype: 'crMoneyField',
                    crColumnDescription: 'Opening Balance',
                    crColumnName: 'openingBalance',
                    anchor:'95%',
                    allowBlank: false
                },{
                    xtype: 'crDateField',
                    crColumnDescription: 'Maturity Date',
                    crColumnName: 'maturityDate',
                    anchor:'95%',
                    allowBlank: true
                },{
                    xtype:'textfield',
                    hidden: true,
                    name: 'range',
                    value: rowSelected.json[2],
                    anchor:'95%'
                }]
            }]
        }],
    dockedItems: [{
    xtype: 'toolbar',
    dock: 'bottom',
    items: [
        { xtype: 'component', flex: 1 },
        { xtype: 'button', text: 'Button 1' }
    ]
}],
    buttons: [{
        text: 'Submit',
        name: 'submit',
        id: uniqueId,
        handler: function(){    
            console.log('button id = ', Ext.getCmp(uniqueId).id);
            console.log('this button id - testid ', uniqueId);       
            insertTermCertificate('form'+uniqueId, uniqueId);
        }
      }]
    }); 

    forms['form'+uniqueId] = formtest;
    console.log('all forms: ', forms);
    
    console.log('testing Id: ' , Ext.getCmp(uniqueId));

    oneStep.push(formtest);
    twoStep = oneStep;
    oneStep = [];       
}
/****************************************************form Panel Details Method End ************************************************/

/****************************************************Insert Term Certificate Start ************************************************/   
function insertTermCertificate(formId, uniqueId){

    var form = forms[formId];
    console.log('submit this form only: ', form);      
    console.log('form Vlaues insertTermCertificate method: ' ,form.getForm().getValues());
    
    var title = form.initialConfig.title;
    var shareId = form.getForm().getValues().savings;   
    var dividendPosting = form.getForm().getValues().dividendPosting;
    var openingBalance = form.getForm().getValues().openingBalance;
    var description = form.getForm().getValues().desc;
    var maturityPosting = form.getForm().getValues().maturityPosting;
    var maturityDate = form.getForm().getValues().maturityDate;
    var maturityFrequency = form.getForm().getValues().maturityFrequency;
    var maturityPeriod = form.getForm().getValues().period;
    var range = form.getForm().getValues().range;

    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');

    var step = xml.addContainer(transaction, 'step');
    var search = xml.addContainer(step, 'search');
    xml.setAttribute(search, 'label', 'SH_TYPE_search');
    xml.addText(search, 'tableName', 'SH_TYPE');
    xml.addText(search, 'filterName', 'BY_DESCRIPTION');
    xml.addOption(search, 'includeSelectColumns', 'Y');
    xml.addOption(search, 'includeTotalHitCount', 'Y');
    xml.addCount(search, 'returnLimit', '10');

    var parameter = xml.addContainer(search, 'parameter');
    xml.addText(parameter, 'columnName', 'DESCRIPTION');
    xml.addText(parameter, 'contents', description);

    CR.Core.ajaxRequest({
      url: 'DirectXMLPostJSON',
      xmlData: xml.getXMLDocument(),
      success: function(response) {
        var searchResponseType = null;
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                } else if (step.search &&
                           step.search.$attr &&
                           step.search.$attr.label === 'SH_TYPE_search') {
                  searchResponseType = step.search;

                }
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
          CR.Core.displayExceptions({ items: errorArray });
        } else {
          console.log('searchResponse', searchResponseType);
          shareTypeSerial(searchResponseType, title,shareId,dividendPosting, openingBalance,description,maturityPosting,maturityDate,maturityFrequency, maturityPeriod, range , uniqueId);
        }
      }
    });

} 
/****************************************************Insert Term Certificate End ************************************************/  

/****************************************************Share Type Serial Method Start *********************************************/ 
function shareTypeSerial(shareTypeSerial, title,shareId,dividendPosting, openingBalance,description,maturityPosting,maturityDate,maturityFrequency, maturityPeriod, range, uniqueId){
   // console.log('share_type_serial: ' ,shareTypeSerial.resultRow[0].serial);
    
    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');

    var step = xml.addContainer(transaction, 'step');
    var search = xml.addContainer(step, 'search');
    xml.setAttribute(search, 'label', 'SHARE_DFLT_search');
    xml.addText(search, 'tableName', 'SHARE_DFLT');
    xml.addText(search, 'filterName', 'BY_TYPE_SERIAL');
    xml.addOption(search, 'includeSelectColumns', 'Y');
    xml.addOption(search, 'includeTotalHitCount', 'Y');
    xml.addCount(search, 'returnLimit', '10');

    var parameter = xml.addContainer(search, 'parameter');
    xml.addText(parameter, 'columnName', 'TYPE_SERIAL');
    xml.addText(parameter, 'contents', shareTypeSerial.resultRow[0].serial);

    CR.Core.ajaxRequest({
      url: 'DirectXMLPostJSON',
      xmlData: xml.getXMLDocument(),
      success: function(response) {
        var searchResponseDefaults = null;
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                } else if (step.search &&
                           step.search.$attr &&
                           step.search.$attr.label === 'SHARE_DFLT_search') {
                  searchResponseDefaults = step.search;
                }
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
          CR.Core.displayExceptions({ items: errorArray });
        } else {
          console.log('searchResponse', searchResponseDefaults);
          defaultsSerial(searchResponseDefaults.resultRow[0].serial, shareTypeSerial.resultRow[0].serial, title, 
                        shareId,dividendPosting, openingBalance,description,maturityPosting,maturityDate,maturityFrequency, maturityPeriod, range, uniqueId);
        }
      }
    });
}   
/**************************************************** Share Type Serial Method End ************************************************/

    /**************************************************** Defaults Serial Method Start ************************************************/     
    function defaultsSerial(shareDefaultSerial, shareTypeSerial , title, shareId,dividendPosting, openingBalance,
                            description,maturityPosting,maturityDate,maturityFrequency, maturityPeriod, range, uniqueId){

        var xml = new CR.XML();
        var sequence = xml.addContainer(xml.getRootElement(),'sequence');
        var transaction = xml.addContainer(sequence,'transaction');

        var step = xml.addContainer(transaction, 'step');
        var search = xml.addContainer(step, 'search');
        xml.setAttribute(search, 'label', 'DIV_CALC_search');
        xml.addText(search, 'tableName', 'DIV_CALC');
        xml.addText(search, 'filterName', 'BY_DESCRIPTION');
        xml.addOption(search, 'includeSelectColumns', 'Y');
        xml.addOption(search, 'includeTotalHitCount', 'Y');
        xml.addCount(search, 'returnLimit', '10');

        var parameter = xml.addContainer(search, 'parameter');
        xml.addText(parameter, 'columnName', 'DESCRIPTION');
        xml.addText(parameter, 'contents', title);

        CR.Core.ajaxRequest({
          url: 'DirectXMLPostJSON',
          xmlData: xml.getXMLDocument(),
          success: function(response) {
            var searchResponse = null;
            var tranResult = 'failed';
            var errorArray = [];
            var responseJson = CR.JSON.parse(response.responseText);
            var query = responseJson.query;
            if (query) {
              Ext.each(query.sequence, function(sequence) {
                Ext.each(sequence.transaction, function(transaction) {
                  tranResult = transaction.$attr.result;
                  Ext.each(transaction.exception, function(exception) {
                    errorArray.push(exception.message);
                  });
                  Ext.each(transaction.step, function(step) {
                    if (step.tranResult &&
                        step.tranResult.category &&
                        step.tranResult.category.option &&
                        step.tranResult.category.option === 'E') {
                      errorArray.push(step.tranResult.description);
                    } else if (step.search &&
                               step.search.$attr &&
                               step.search.$attr.label === 'DIV_CALC_search') {
                      searchResponse = step.search;
                    }
                  });
                });
              });
            }
            if (tranResult !== 'posted' || errorArray.length > 0) {
              CR.Core.displayExceptions({ items: errorArray });
            } else {
              console.log('searchResponse', searchResponse);
              termCertificate(searchResponse.resultRow[0].serial,shareDefaultSerial, shareTypeSerial , title , shareId,dividendPosting, 
                                openingBalance,description,maturityPosting,maturityDate,maturityFrequency, maturityPeriod, range, uniqueId);     
            }
          }
        });
    }
    /****************************************************Defaults Serial Method End ************************************************/      

    /************************************** TermCertificate Method  Start *********************************************************/   
    function termCertificate(dividendCalculationSerial,shareDefaultSerial, shareTypeSerial , 
                            title, shareId,dividendPosting, openingBalance,description,maturityPosting,maturityDate,maturityFrequency, maturityPeriod, range, uniqueId){                                
        
        if(dividendPosting === 'Deposit'){
           dividendPosting = 'D';
        } else if(dividendPosting === 'Transfer'){
           dividendPosting = 'T';
        }
        
        if(maturityPosting === 'Renew'){
            maturityPosting = 'R';
        } else if(maturityPosting === 'Transfer then close'){
            maturityPosting = 'T';
        }
        
        if(maturityFrequency === 'Months'){
           maturityFrequency = 'M';
        } else if(maturityFrequency === 'Days'){
           maturityFrequency = 'D';
        }
        
        ////////////////////////////////////////////////////////////////Opening Balance - Start////////////////////////////////////////////////////////////////////////////////////////////
        var openingBalanceRange;
       
        var minimumBalance;
        //title === 'IRA Term Certificate 12 Month' || title === 'IRA Term Certificate 18 Month' || title ==='IRA Term Certificate 24 Month' || title ==='IRA Term Certificate 60-120 Month' 
        //title ==='Term Certificate 12-23 Month MTH DIV' || title ==='Term Certificate 24-35 Month MTH DIV' || title ==='Term Certificate 3-5 Month MTH DIV' || title ==='Term Certificate 36-59 Month MTH DIV' ||
        //title ==='Term Certificate 6-11 Month MTH DIV' || title === 'Term Certificate 60-120 Month MTH DIV' ||
        //title === 'Term Certificate Opt Rate 13 Mo MTH DIV' || title ===  'Term Certificate Opt Rate 24 Mo MTH DIV' || title === 'Term Certificate Opt Rate 36 Mo MTH DIV' ||
        if((title === 'Term Certificate 12-23 Month QTR DIV' ||  title ==='Term Certificate 24-35 Month QTR DIV' || 
            title === 'Term Certificate 3-5 Month QTR DIV' ||  title ==='Term Certificate 36-59 Month QTR DIV' || 
            title === 'Term Certificate 6-11 Month QTR DIV' ||  title === 'Term Certificate 60-120 Month QTR DIV')){
            minimumBalance = 1000;
            openingBalanceRange = CR.MoneyField.convertFromDisplay(openingBalance) >= minimumBalance  ? openingBalance: minimumValue(minimumBalance) ;
        } else if(title === 'Term Certificate Add-On 12 Month QTR DIV' ){
            minimumBalance = 250;
            openingBalanceRange = CR.MoneyField.convertFromDisplay(openingBalance) >= minimumBalance  ? openingBalance: minimumValue(minimumBalance) ;
        } else if(( title === 'Term Certificate Opt Rate 13 Mo QTR DIV' || title === 'Term Certificate Opt Rate 24 Mo QTR DIV' || 
            title === 'Term Certificate Opt Rate 36 Mo QTR DIV') ){
            minimumBalance = 10000;
            openingBalanceRange = CR.MoneyField.convertFromDisplay(openingBalance) >= minimumBalance  ? openingBalance: minimumValue(minimumBalance) ;
        } 
        function minimumValue(val){
            Ext.Msg.alert('Opening Balance', 'The minimum Opening Balance for <b>' +title+ '</b> product is <b style="color: #ff0000;">'+Ext.util.Format.number(val, '$0,000.00')+'</b>');
        }
    
        ////////////////////////////////////////////////////////////////Opening Balance - End////////////////////////////////////////////////////////////////////////////////////////////
        
        
        ////////////////////////////////////////////////////////////////Maturity Period - Start////////////////////////////////////////////////////////////////////////////////////////////
        var maturityPeriodRange;
        
        if(range === '12' && maturityPeriod <= 12){          
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '13' && maturityPeriod <= 13 ){
            maturityPeriodRange = maturityPeriod; 
        }/*else if(range === '18'  && maturityPeriod <= 18 ){
            maturityPeriodRange = maturityPeriod; 
        }*/else if(range === '24' && maturityPeriod <= 24 ){
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '36' && maturityPeriod <= 36 ){
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '3-5' && maturityPeriod >= 3 && maturityPeriod <= 5 ){ 
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '6-11' && maturityPeriod >= 6 && maturityPeriod <= 11 ){
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '12-23'  && maturityPeriod >= 12 && maturityPeriod <= 23 ){
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '24-35'  && maturityPeriod >= 24 && maturityPeriod <= 35 ){
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '36-59'  &&  maturityPeriod >= 36 && maturityPeriod <= 59 ){
            maturityPeriodRange = maturityPeriod; 
        }else if(range === '60-120' && maturityPeriod >= 60 && maturityPeriod <= 120 ){
            maturityPeriodRange = maturityPeriod; 
        }else{
            Ext.Msg.alert('Maturity Period', 'You must select a maturity period within the range for this product');
        }       
        
        ////////////////////////////////////////////////////////////////Maturity Period - End////////////////////////////////////////////////////////////////////////////////////////////
        
        
        ////////////////////////////////////////////////////////////////Maturity Date - Start////////////////////////////////////////////////////////////////////////////////////////////
        function changeDateFormat(maturityDate){  // expects Y-m-d m/d/Y 01/17/2018
        var splitDate = maturityDate.split('/');
        if(splitDate.count === 0){
            return null;
        }
        var year = splitDate[2];
        var month = splitDate[0];
        var day = splitDate[1]; 
        return year + '-' + month + '-' + day;
        }
        var maturityRangeDate;
        var maturity = changeDateFormat(maturityDate);
       // console.log('maturity: ', maturity);
        
        var dateCurrent = new Date();
        var currentDateValue = dateCurrent.format('Y-m-d');
        
       // console.log('range: ' , range);
        
        var threeMonth = new Date();
        threeMonth.setMonth(threeMonth.getMonth() + 3);       
        threeMonth = threeMonth.format('Y-m-d');
        
        var fiveMonth = new Date();  var sixMonth = new Date();      
        fiveMonth.setMonth(fiveMonth.getMonth() + 6);   
        fiveMonth.setDate(fiveMonth.getDate() - 1);       
        fiveMonth = fiveMonth.format('Y-m-d');
        sixMonth.setMonth(sixMonth.getMonth() + 6); 
        sixMonth = sixMonth.format('Y-m-d');
        
        var twelveMonth = new Date(); var elevenMonth = new Date();
        twelveMonth.setMonth(twelveMonth.getMonth() + 12);       
        twelveMonth = twelveMonth.format('Y-m-d');
        elevenMonth.setMonth(elevenMonth.getMonth() + 12);   
        elevenMonth.setDate(elevenMonth.getDate() - 1);       
        elevenMonth = elevenMonth.format('Y-m-d');       
        
        var thirteenMonth = new Date();
        thirteenMonth.setMonth(thirteenMonth.getMonth() + 13);       
        thirteenMonth = thirteenMonth.format('Y-m-d');
        
        var eighteenMonth = new Date();
        eighteenMonth.setMonth(eighteenMonth.getMonth() + 18);       
        eighteenMonth = eighteenMonth.format('Y-m-d');
        
        var twentyFourMonth = new Date(); var twentyThreeMonth = new Date();
        twentyFourMonth.setMonth(twentyFourMonth.getMonth() + 24);       
        twentyFourMonth = twentyFourMonth.format('Y-m-d');
        twentyThreeMonth.setMonth(twentyThreeMonth.getMonth() + 24);   
        twentyThreeMonth.setDate(twentyThreeMonth.getDate() - 1);       
        twentyThreeMonth = twentyThreeMonth.format('Y-m-d');
        
        var thirtySixMonth = new Date(); var thirtyFiveMonth = new Date();
        thirtySixMonth.setMonth(thirtySixMonth.getMonth() + 36);       
        thirtySixMonth = thirtySixMonth.format('Y-m-d');
        thirtyFiveMonth.setMonth(thirtyFiveMonth.getMonth() + 36);   
        thirtyFiveMonth.setDate(thirtyFiveMonth.getDate() - 1);       
        thirtyFiveMonth = thirtyFiveMonth.format('Y-m-d');
        
        var sixtyMonth = new Date(); var fiftyNineMonth = new Date();
        sixtyMonth.setMonth(sixtyMonth.getMonth() + 60);       
        sixtyMonth = sixtyMonth.format('Y-m-d');
        fiftyNineMonth.setMonth(fiftyNineMonth.getMonth() + 60);   
        fiftyNineMonth.setDate(fiftyNineMonth.getDate() - 1);       
        fiftyNineMonth = fiftyNineMonth.format('Y-m-d');
        
        var oneTwentyMonth = new Date();
        oneTwentyMonth.setMonth(oneTwentyMonth.getMonth() + 120);       
        oneTwentyMonth = oneTwentyMonth.format('Y-m-d');              
        
        if(maturityDate){     
            if(range === '12'   && maturity > currentDateValue  && maturity <= twelveMonth){ 
                maturityRangeDate = maturity; 
            }else if(range === '13'     && maturity > currentDateValue  && maturity <= thirteenMonth ){
                maturityRangeDate = maturity; 
            }else if(range === '18'     && maturity > currentDateValue  && maturity <= eighteenMonth ){
                maturityRangeDate = maturity; 
            }/*else if(range === '24'     && maturity > currentDateValue  && maturity <= twentyFourMonth ){
                maturityRangeDate = maturity; 
            }*/else if(range === '36'     && maturity > currentDateValue  && maturity <= thirtySixMonth ){
                maturityRangeDate = maturity; 
            }else if(range === '3-5'    && maturity >= threeMonth       && maturity <= fiveMonth ){ 
                maturityRangeDate = maturity; 
            }else if(range === '6-11'   && maturity >= sixMonth         && maturity <= elevenMonth ){
                maturityRangeDate = maturity; 
            }else if(range === '12-23'  && maturity >= twelveMonth      && maturity <= twentyThreeMonth ){
                maturityRangeDate = maturity; 
            }else if(range === '24-35'  && maturity >= twentyFourMonth  && maturity <= thirtyFiveMonth ){
                maturityRangeDate = maturity; 
            }else if(range === '36-59'  && maturity >= thirtySixMonth   && maturity <= fiftyNineMonth ){
                maturityRangeDate = maturity; 
            }else if(range === '60-120' && maturity >= sixtyMonth       && maturity <= oneTwentyMonth ){
                maturityRangeDate = maturity; 
            } else {
                 Ext.Msg.alert('Maturity Date', 'You must select a maturity date within '+ range +' months for this product');
                   maturityDate = false;
            }
       } else{
           maturityDate = true;
       }      

        ////////////////////////////////////////////////////////////////Maturity Date - End////////////////////////////////////////////////////////////////////////////////////////////        
                      
        var xml = new CR.XML();
        var sequence = xml.addContainer(xml.getRootElement(),'sequence');
        var transaction = xml.addContainer(sequence,'transaction');

        var step = xml.addContainer(transaction, 'step');
        var record = xml.addContainer(step, 'record');
        xml.setAttribute(record, 'label', 'SHARE_record');
        xml.addText(record, 'tableName', 'SHARE');
        xml.addOption(record, 'operation', 'I');
        xml.addText(record, 'targetParentSerial', accountSerial);
        xml.addText(record, 'defaultsSerial', shareDefaultSerial);
        xml.addOption(record, 'includeTableMetadata', 'N');
        xml.addOption(record, 'includeColumnMetadata', 'N');
        xml.addOption(record, 'includeRowDescriptions', 'Y');
        xml.addOption(record, 'includeAllColumns', 'N');
        xml.addOption(record, 'newLocationOption', 'L');

        var field = null;
        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'ID');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', shareId);

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'DESCRIPTION');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', description);

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'DIVIDEND_CALCULATION_SERIAL');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', dividendCalculationSerial);

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'DIVIDEND_POSTING');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', dividendPosting);
        
        if(openingBalanceRange){
           field = xml.addContainer(record, 'field');
            xml.addText(field, 'columnName', 'OPENING_BALANCE');
            xml.addOption(field, 'operation', 'S');
            xml.addText(field, 'newContents', CR.MoneyField.convertFromDisplay(openingBalanceRange)); 
        }
                
        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'MATURITY_POSTING');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', maturityPosting);

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'MATURITY_FREQUENCY');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', maturityFrequency);

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'STMT_CUTOFF_GROUP_SERIAL');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', 102); //STMT_CUTOFF_GROUP - Statement Cutoff Group: Quarterly Statement Cutoff Group  (It is same for all the term certificates)

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'TYPE_SERIAL');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', shareTypeSerial); // SH_TYPE  - Type:
        
        if(maturityPeriodRange){
            field = xml.addContainer(record, 'field');
            xml.addText(field, 'columnName', 'MATURITY_PERIOD');
            xml.addOption(field, 'operation', 'S');
            xml.addText(field, 'newContents', maturityPeriodRange);
        }                
        
        if(maturityRangeDate){
            field = xml.addContainer(record, 'field');
            xml.addText(field, 'columnName', 'MATURITY_DATE');
            xml.addOption(field, 'operation', 'S');
            xml.addText(field, 'newContents', maturityRangeDate);
        }
        
        if(openingBalanceRange && maturityPeriodRange && maturityDate){  //start
            CR.Core.ajaxRequest({
              url: 'DirectXMLPostJSON',
              xmlData: xml.getXMLDocument(),
              success: function(response) {
                var recordResponse = null;
                var tranResult = 'failed';
                var errorArray = [];
                var responseJson = CR.JSON.parse(response.responseText);
                var query = responseJson.query;
                if (query) {
                  Ext.each(query.sequence, function(sequence) {
                    Ext.each(sequence.transaction, function(transaction) {
                      tranResult = transaction.$attr.result;
                      Ext.each(transaction.exception, function(exception) {
                        errorArray.push(exception.message);
                      });
                      Ext.each(transaction.step, function(step) {
                        if (step.tranResult &&
                            step.tranResult.category &&
                            step.tranResult.category.option &&
                            step.tranResult.category.option === 'E') {
                          errorArray.push(step.tranResult.description);
                        } else if (step.record &&
                                   step.record.$attr &&
                                   step.record.$attr.label === 'SHARE_record') {
                                recordResponse = step.record;
                        }
                      });
                    });
                  });
                }
                if (tranResult !== 'posted' || errorArray.length > 0) {
                  CR.Core.displayExceptions({ items: errorArray });
                } else {
                        console.log('Term Certificate Response: ', recordResponse);
                        Ext.Msg.alert('Insert Term Certificate', 'The <b>'+ recordResponse.rowDescription +'</b> record has been inserted successfully.');
                        console.log('Unique Id testing: ', uniqueId);
                        var test = uniqueId;
                        Ext.getCmp(uniqueId).hide(); //hiding the button once the form is submitted.
                        console.log('tools: ' ,formtest.tools[test.toString()]);
                        formtest.tools[test].hide();
                       //formtest.tools.hide(); 
                        
                       // console.log('tools Id: ' ,formtest.tools);
                       
                        console.log('tool class: ', formtest.tools[test].dom.className);
                        console.log('tools Id: ', formtest.tools[test].dom.id);
                        
                        var className = formtest.tools[test].dom.className;
                        //var id =  formtest.tools[test].dom.id;
                       // console.log('id: ', id.toString());
                      //  var stringTesting = id.toString();
                       // console.log('stringTesting: ', stringTesting);

                            console.log('generated Id: ', formtest.tools[test].id);
                           // formtest.tools[test].id.hide();
                             console.log('true');


                        var targetSerial = recordResponse.targetSerialResult;
                        
                        if(recordResponse.field[3].newContentsDescription === 'Transfer' && recordResponse.field[5].newContentsDescription === 'Transfer then close'){
                            savingsTransferMaturityPosting(targetSerial , title);
                            savingsTransferDividendPosting(targetSerial , title);
                        } else if(recordResponse.field[3].newContentsDescription === 'Transfer'){
                            savingsTransferDividendPosting(targetSerial , title);
                        } else if(recordResponse.field[5].newContentsDescription === 'Transfer then close'){
                            savingsTransferMaturityPosting(targetSerial , title);
                        } else {
                            letsGetOutofHere(navigation_data.complete);
                        }
                }
              }
            });
        } //end
    } 
    /************************************** TermCertificate Method  End *********************************************************/
    
    var transferMaturityPanel; var transferDividendPanel ; var arrayForMaturity = []; var arrayForDividend = [];
    
    /************************************** Savings Transfer for Maturity Posting Method Start **********************************/
    function savingsTransferMaturityPosting(targetSerialId , title){
        
        console.log('targetSerial Id before form: ' , targetSerialId);

        transferMaturityPanel =  new CR.FormPanel({
            renderTo: Ext.getBody(),   
            labelWidth: 175,
            title: 'Maturity Savings Transfer for '+title,
            bodyStyle:'padding:5px 5px 0',
            collapsible: true,
            id: targetSerialId,
            region: 'east',
            margins: '0 0 0 5',
            x:305,
            style: {position:'relative' , width: 700},
            items: [
                {
                    xtype: 'crTextField',      
                    crColumnDescription: 'Share Serial',
                    name: 'shareSerial',
                    crColumnName: 'cuTextField',
                    hidden: true ,
                    disabled: true,
                    crContents: targetSerialId
                }, 
                {
                    xtype:'crOptionField',
                    crColumnDescription: 'Category',
                    crColumnName: 'category',
                    crRowDescription : 'M',
                    crOptions : [
                                    ["M","Maturity"]
                                ],
                    disabled: true            
                },{
                    xtype:'crOptionField',
                    crColumnDescription: 'Method',
                    crColumnName: 'method',
                    crRowDescription : 'S',
                    crOptions : [
                                    ["S","Savings transfer"]
                                ],
                    disabled: true            
                },{
                    xtype: 'crSerialField',
                    crColumnDescription: 'Savings',                    
                    crTableName: 'SHARE',
                    crColumnName : 'savings',
                    crSearchFilterName: 'BY_PARENT_SERIAL',
                    crSearchParameters: [{
                            columnName: 'PARENT_SERIAL',
                            contents: accountSerial
                    }],
                    crNullAllowed: true,
                    required : true,
                    crIncludeDescriptionParameter: false
                }
            ]
        }); 
        
        //console.log('For Id testing: ' , Ext.getCmp(targetSerialId));        
        arrayForMaturity.push(Ext.getCmp(targetSerialId));
                  
    }
    /****************************************************Savings Transfer for Maturity Posting Method End *******************************************/
    
    /****************************************************Savings Transfer for Dividend Posting Method Start *****************************************/
    function savingsTransferDividendPosting(targetSerialId , title){
        console.log('I am here Dividend');
        transferDividendPanel =  new CR.FormPanel({
            renderTo: Ext.getBody(),   
            labelWidth: 175,
            title: 'Dividend Savings Transfer for '+title,
            bodyStyle:'padding:5px 5px 0',
            collapsible: true,
            id: targetSerialId,
            region: 'east',
            margins: '0 0 0 5',
            x:305,
            style: {position:'relative' , width: 700},
            items: [
                {
                    xtype: 'crTextField',      
                    crColumnDescription: 'Share Serial',
                    name: 'shareSerial',
                    crColumnName: 'cuTextField',
                    hidden: true ,
                    disabled: true,
                    crContents: targetSerialId
                }, 
                {
                    xtype:'crOptionField',
                    crColumnDescription: 'Category',
                    crColumnName: 'category',
                    crRowDescription : 'V',
                    crOptions : [
                                    ["V","Dividend"]
                                ],
                    disabled: true            
                },{
                    xtype:'crOptionField',
                    crColumnDescription: 'Method',
                    crColumnName: 'method',
                    crRowDescription : 'S',
                    crOptions : [
                                    ["S","Savings transfer"]
                                ],
                    disabled: true            
                },{
                    xtype: 'crSerialField',
                    crColumnDescription: 'Savings',                    
                    crTableName: 'SHARE',
                    crColumnName : 'savings',
                    crSearchFilterName: 'BY_PARENT_SERIAL',
                    crSearchParameters: [{
                            columnName: 'PARENT_SERIAL',
                            contents: accountSerial
                    }],
                    crNullAllowed: true,
                    required : true,
                    crIncludeDescriptionParameter: false
                }
            ]
        });       
        arrayForDividend.push(Ext.getCmp(targetSerialId));     
    }
    /************************************************* Savings Transfer for Dividend Posting Method End *************************************/
    
    /************************************************* Insert Savings Transfer for Maturity Posting Start ******************************************/    
       var targetParentSerial ; var maturitySerialArray = []; var test2 ; var test3= []; var test4 = []; var test5 = []; var object = {};
    function insertMaturitySavingsTransfer(){
        var shareSerialForMaturity, targetParentSerialForMaturity, descriptionForMaturity , shareSerialForDividend = 0, targetParentSerialForDividend, descriptionForDividend ,   submit = true;     
        console.log('Maturity Form: ' ,arrayForMaturity);
        console.log('Dividend Form: ' ,arrayForDividend);
        if(arrayForMaturity.length > 0 && arrayForDividend.length > 0 && arrayForDividend.id === arrayForMaturity.id){
            arrayForMaturity.forEach(function (entry){
                targetParentSerialForMaturity = entry.items.items[0].crContents;
                descriptionForMaturity = entry.items.items[3].crRowDescription;
                share.forEach(function (entry){     
                    if(descriptionForMaturity === entry.rowDescription){
                        shareSerialForMaturity = entry.targetSerial; 
                    }
                });
               // console.log('share serial: ', shareSerialForMaturity);
                
                if(shareSerialForMaturity){
                    targetParentSerial = targetParentSerialForMaturity;
                    //console.log('test: ' , test);
                    maturitySerialArray.push(targetParentSerial);
                   // console.log('test1[0]:' , test1[0]);                    
                   // console.log('test1: ' , test1);
                    targetParentSerial = '';
                   // console.log('test at end: ' , test);
                    var i;
                    if(maturitySerialArray.length === 1){
                        testMaturity(maturitySerialArray[0], shareSerialForMaturity);
                    } else if(maturitySerialArray.length > 1){
                        for(i=1; i< maturitySerialArray.length; i++){
                            if(maturitySerialArray[0] === maturitySerialArray[i]){
                                console.log('same target parent serial');
                            }
                        }
                    }                   
                }
                else{
                    Ext.Msg.alert('Insert the value', 'Insert Maturity Savings Transfer value');
                }
            });
            
          
            arrayForDividend.forEach(function (entry){
                targetParentSerialForDividend = entry.items.items[0].crContents;
                descriptionForDividend = entry.items.items[3].crRowDescription;
 
                share.forEach(function (entry){     
                    if(descriptionForDividend === entry.rowDescription){
                        shareSerialForDividend = entry.targetSerial; 
                    }
                });
                
                if(shareSerialForDividend){
                    test2 = targetParentSerialForDividend;
                    //console.log('test: ' , test);
                    test3.push(test2);
                   // console.log('test1[0]:' , test1[0]);                    
                    console.log('test1: ' , test3);
                    test2 = '';
                   // console.log('test at end: ' , test);
                    var i;
                    if(test3.length === 1){
                        testDividend(test3[0],shareSerialForDividend); 
                    } else if(test3.length > 1){
                        for(i=1; i< test3.length; i++){
                            if(test3[0] === test3[i]){
                               console.log('same target serial'); 
                            }/*else if(test3[0] !== test3[i]){
                                var target = test3[i];
                                testDividend(target,shareSerialForDividend); 
                            }*/
                        }
                    }                   
                }
                else{
                    Ext.Msg.alert('Insert the value', 'Insert Dividend Savings Transfer value');
                }
                   
            });
            
            if(shareSerialForMaturity && shareSerialForDividend){
                letsGetOutofHere(navigation_data.complete);
            }
            
           // letsGetOutofHere(navigation_data.complete);
        } else if(arrayForMaturity.length > 0 && arrayForDividend.length > 0 ){
            arrayForMaturity.forEach(function (entry){
                targetParentSerialForMaturity = entry.items.items[0].crContents;
                descriptionForMaturity = entry.items.items[3].crRowDescription;
                share.forEach(function (entry){
                    
                    if(descriptionForMaturity === entry.rowDescription){
                        shareSerialForMaturity = entry.targetSerial; 
                    }
                });
               // console.log('share serial: ', shareSerialForMaturity);
                
                if(shareSerialForMaturity){
                    targetParentSerial = targetParentSerialForMaturity;
                    //console.log('test: ' , test);
                    maturitySerialArray.push(targetParentSerial);
                   // console.log('test1[0]:' , test1[0]);                    
                    console.log('test1: ' , maturitySerialArray);
                    targetParentSerial = '';
                   // console.log('test at end: ' , test);
                    var i;
                    if(maturitySerialArray.length === 1){
                        testMaturity(maturitySerialArray[0], shareSerialForMaturity);
                    } else if(maturitySerialArray.length > 1){
                        for(i=1; i< maturitySerialArray.length; i++){
                            if(maturitySerialArray[0] !== maturitySerialArray[i]){
                                //console.log('test1[0]:' , test1[0]);
                              //  console.log('test1[i+1] secodn time:' , test1[i]);
                                var target = maturitySerialArray[i];
                              //  console.log('printing the target: ', target);
                               testMaturity(target, shareSerialForMaturity); 
                            }
                        }
                    }                   
                }
                else{
                    Ext.Msg.alert('Insert Maturity Savings Transfer', 'Insert the value');
                }
            });
            
          
            arrayForDividend.forEach(function (entry){
                targetParentSerialForDividend = entry.items.items[0].crContents;
                descriptionForDividend = entry.items.items[3].crRowDescription;
 
                share.forEach(function (entry){     
                    if(descriptionForDividend === entry.rowDescription){
                        shareSerialForDividend = entry.targetSerial; 
                    }
                });
                
                if(shareSerialForDividend){
                    test2 = targetParentSerialForDividend;
                    test3.push(test2);             
                    console.log('test1: ' , test3);
                    test2 = '';
                    var i;
                    if(test3.length === 1){
                        testDividend(test3[0],shareSerialForDividend); 
                    } else if(test3.length > 1){
                        for(i=1; i< test3.length; i++){
                            if(test3[0] !== test3[i]){
                                var target = test3[i];
                                testDividend(target,shareSerialForDividend); 
                            }
                        }
                    }                   
                }
                else{
                    Ext.Msg.alert('Insert the value', 'Insert Dividend Savings Transfer value');
                }
                
                
                
                              
            });
            
            if(shareSerialForMaturity && shareSerialForDividend){
                letsGetOutofHere(navigation_data.complete);
            }
            
           // letsGetOutofHere(navigation_data.complete);
        }
        
        else if(arrayForMaturity.length > 0){
            
           /* arrayForMaturity.forEach(function (entry){
                targetParentSerialForMaturity = entry.items.items[0].crContents;
                descriptionForMaturity = entry.items.items[3].crRowDescription;
 
                share.forEach(function (entry){
                    console.log('times');
                    if(descriptionForMaturity === entry.rowDescription){
                        shareSerialForMaturity = entry.targetSerial; 
                    }
                }); 
                
                test4.push(targetParentSerialForMaturity);
                 
                console.log('test5: ', test5);
                if(shareSerialForMaturity){    
                    testMaturity(targetParentSerialForMaturity, shareSerialForMaturity);
                } else{
                    Ext.Msg.alert('Insert Maturity Savings Transfer', 'Insert the value');
                }

            });
            
            if(shareSerialForMaturity){
                console.log('success');
                letsGetOutofHere(navigation_data.complete);
            } else {
                console.log('failure');
            }*/
            
            
            arrayForMaturity.forEach(function (entry){
                targetParentSerialForMaturity = entry.items.items[0].crContents;
                descriptionForMaturity = entry.items.items[3].crRowDescription;
                console.log('description:' , descriptionForMaturity);
                
                object[targetParentSerialForMaturity] = {};
                object[targetParentSerialForMaturity]['targetParentSerialForMaturity'] = entry.items.items[0].crContents;
 
                share.forEach(function (entry){
                    if(descriptionForMaturity.length > 0 &&  descriptionForMaturity === entry.rowDescription){                    
                        shareSerialForMaturity = entry.targetSerial;  
                        object[targetParentSerialForMaturity]['shareSerialForMaturity'] = shareSerialForMaturity;
                    }
                });
                
                console.log('Outside shareSerialForMaturity: ' , shareSerialForMaturity);
               
            });
            
            console.log('Object Values: ' , object);
            
            for(var key in object){
                var value = object[key];              
                console.log('shareSerialForMaturity:', value.shareSerialForMaturity);
                console.log('targetParentSerialForMaturity:' , value.targetParentSerialForMaturity);
                if(!value.shareSerialForMaturity){
                    Ext.Msg.alert('Insert the value', 'Insert Maturity Savings Transfer value');
                    submit = false;
                } 
                console.log('testing value: ' , value);
            }
            
            if(submit === true){
                console.log('when ready: ' , object);
                for(var key in object){
                var value = object[key];
                testMaturity(value.targetParentSerialForMaturity,value.shareSerialForMaturity);                
            }             
            letsGetOutofHere(navigation_data.complete);
            }
            
        } else if(arrayForDividend.length > 0){
            arrayForDividend.forEach(function (entry){
                targetParentSerialForDividend = entry.items.items[0].crContents;
                descriptionForDividend = entry.items.items[3].crRowDescription;
                console.log('description:' , descriptionForDividend);
                
                object[targetParentSerialForDividend] = {};
                object[targetParentSerialForDividend]['targetParentSerialForDividend'] = entry.items.items[0].crContents;
 
                share.forEach(function (entry){
                    if(descriptionForDividend.length > 0 &&  descriptionForDividend === entry.rowDescription){                    
                        shareSerialForDividend = entry.targetSerial;  
                        object[targetParentSerialForDividend]['shareSerialForDividend'] = shareSerialForDividend;
                    }
                });
                
                console.log('Outside shareSerialForDividend: ' , shareSerialForDividend);
               
            });
            
            console.log('Object Values: ' , object);
            
            for(var key in object){
                var value = object[key];              
                console.log('shareSerialForDividend:', value.shareSerialForDividend);
                console.log('targetParentSerialForDividend:' , value.targetParentSerialForDividend);
                if(!value.shareSerialForDividend){
                    Ext.Msg.alert('Insert the value', 'Insert Dividend Savings Transfer value');
                    submit = false;
                } 
                console.log('testing value: ' , value);
            }
            
            if(submit === true){
                console.log('when ready: ' , object);
                for(var key in object){
                var value = object[key];
                testDividend(value.targetParentSerialForDividend,value.shareSerialForDividend);                
            }             
            letsGetOutofHere(navigation_data.complete);
            }
        } else{
             letsGetOutofHere(navigation_data.complete); 
        }   
        
    }
    
    
   
    function testMaturity(targetParentSerial, shareSerial){
    
        var xml = new CR.XML();
        var sequence = xml.addContainer(xml.getRootElement(),'sequence');
        var transaction = xml.addContainer(sequence,'transaction');

        var step = xml.addContainer(transaction, 'step');
        var record = xml.addContainer(step, 'record');
        xml.setAttribute(record, 'label', 'SH_TRANSFER_record');
        xml.addText(record, 'tableName', 'SH_TRANSFER');
        xml.addOption(record, 'operation', 'I');
        xml.addText(record, 'targetParentSerial', targetParentSerial);
        xml.addOption(record, 'includeTableMetadata', 'N');
        xml.addOption(record, 'includeColumnMetadata', 'N');
        xml.addOption(record, 'includeRowDescriptions', 'Y');
        xml.addOption(record, 'includeAllColumns', 'N');

        var field = null;
        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'CATEGORY');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', 'M');

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'METHOD');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', 'S');

        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'SHARE_SERIAL');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', shareSerial);       

        CR.Core.ajaxRequest({
          url: 'DirectXMLPostJSON',
          xmlData: xml.getXMLDocument(),
          success: function(response) {
            var recordResponse = null;
            var tranResult = 'failed';
            var errorArray = [];
            var responseJson = CR.JSON.parse(response.responseText);
            var query = responseJson.query;
            if (query) {
              Ext.each(query.sequence, function(sequence) {
                Ext.each(sequence.transaction, function(transaction) {
                  tranResult = transaction.$attr.result;
                  Ext.each(transaction.exception, function(exception) {
                    errorArray.push(exception.message);
                  });
                  Ext.each(transaction.step, function(step) {
                    if (step.tranResult &&
                        step.tranResult.category &&
                        step.tranResult.category.option &&
                        step.tranResult.category.option === 'E') {
                      errorArray.push(step.tranResult.description);
                    } else if (step.record &&
                               step.record.$attr &&
                               step.record.$attr.label === 'SH_TRANSFER_record') {
                      recordResponse = step.record;
                    }
                  });
                });
              });
            }
            if (tranResult !== 'posted' || errorArray.length > 0) {
                CR.Core.displayExceptions({ items: errorArray });
            } else {
                console.log('Insert Maturity Savings Transfer: ', recordResponse);
                //Ext.Msg.alert('Insert Confirmation', 'The <b>'+ recordResponse.rowDescription +'</b> record has been inserted successfully.');
               // letsGetOutofHere(navigation_data.complete); 
            }
          }
        });
    }
    
    /***********************************************Insert Savings Transfer for Maturity Posting End *************************************/ 
   


    /***********************************************Insert Savings Transfer for Dividend Posting Start ************************************/
    
    
    function testDividend(targetParentSerial, shareSerial){
        var xml = new CR.XML();
                var sequence = xml.addContainer(xml.getRootElement(),'sequence');
                var transaction = xml.addContainer(sequence,'transaction');

                var step = xml.addContainer(transaction, 'step');
                var record = xml.addContainer(step, 'record');
                xml.setAttribute(record, 'label', 'SH_TRANSFER_record');
                xml.addText(record, 'tableName', 'SH_TRANSFER');
                xml.addOption(record, 'operation', 'I');
                xml.addText(record, 'targetParentSerial', targetParentSerial);
                xml.addOption(record, 'includeTableMetadata', 'N');
                xml.addOption(record, 'includeColumnMetadata', 'N');
                xml.addOption(record, 'includeRowDescriptions', 'Y');
                xml.addOption(record, 'includeAllColumns', 'N');

                var field = null;
                field = xml.addContainer(record, 'field');
                xml.addText(field, 'columnName', 'CATEGORY');
                xml.addOption(field, 'operation', 'S');
                xml.addText(field, 'newContents', 'V');

                field = xml.addContainer(record, 'field');
                xml.addText(field, 'columnName', 'METHOD');
                xml.addOption(field, 'operation', 'S');
                xml.addText(field, 'newContents', 'S');

                field = xml.addContainer(record, 'field');
                xml.addText(field, 'columnName', 'SHARE_SERIAL');
                xml.addOption(field, 'operation', 'S');
                xml.addText(field, 'newContents', shareSerial);       

                CR.Core.ajaxRequest({
                  url: 'DirectXMLPostJSON',
                  xmlData: xml.getXMLDocument(),
                  success: function(response) {
                    var recordResponse = null;
                    var tranResult = 'failed';
                    var errorArray = [];
                    var responseJson = CR.JSON.parse(response.responseText);
                    var query = responseJson.query;
                    if (query) {
                      Ext.each(query.sequence, function(sequence) {
                        Ext.each(sequence.transaction, function(transaction) {
                          tranResult = transaction.$attr.result;
                          Ext.each(transaction.exception, function(exception) {
                            errorArray.push(exception.message);
                          });
                          Ext.each(transaction.step, function(step) {
                            if (step.tranResult &&
                                step.tranResult.category &&
                                step.tranResult.category.option &&
                                step.tranResult.category.option === 'E') {
                              errorArray.push(step.tranResult.description);
                            } else if (step.record &&
                                       step.record.$attr &&
                                       step.record.$attr.label === 'SH_TRANSFER_record') {
                              recordResponse = step.record;
                            }
                          });
                        });
                      });
                    }
                    if (tranResult !== 'posted' || errorArray.length > 0) {
                        CR.Core.displayExceptions({ items: errorArray });
                    } else {
                        console.log('Insert Dividend Savings Transfer: ', recordResponse);
                        //Ext.Msg.alert('Insert Confirmation', 'The <b>'+ recordResponse.rowDescription +'</b> record has been inserted successfully.');
                       // letsGetOutofHere(navigation_data.complete);
                    }
                  }
                });
    }
    
    /**************************************Insert Savings Transfer for Dividend Posting End *****************************************/

}
/**************************************************** Account Number Details Method End ************************************************/

});
/****************************************************onReady Method End ************************************************/